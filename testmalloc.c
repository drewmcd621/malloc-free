#include "mymalloc.h"


void WalkMem();


int main()
{

	
	
	//Step 1
	int *a = my_nextfit_malloc(60);
	int *b = my_nextfit_malloc(40);
	int *c = my_nextfit_malloc(85);
	int *d = my_nextfit_malloc(26);
	int *e = my_nextfit_malloc(70);
	
	WalkMem();
	

	//Step 2
	my_free(b);
	my_free(d);
	
	WalkMem();
	
	//Step 3
	my_free(c);
	
	WalkMem();
	
	//Step 4
	int *f = my_nextfit_malloc(62);
	
	WalkMem();
	
	//Step 5
	my_free(e);
	
	WalkMem();
	
	//Step 6
	my_free(f);
	my_free(a);

	
	WalkMem();
	
	printf("\nDone\n");

}