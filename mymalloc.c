

#include <unistd.h>

#define MIN_SIZE 10

//Linked list structure
typedef struct mem_list {
	unsigned short ident;
	struct mem_list *next;
	struct mem_list *prev;
	int size;
	unsigned short used;
} mem_list;

//Prototypes
void* *my_nextfit_malloc(int size);
mem_list* nextFit(int size);
mem_list* incHeap(int size);
int my_free(void* item);


//Globals
static mem_list* _nextfit_ = NULL;
static mem_list* _max_ = NULL;
static mem_list* _min_ = NULL;



void* *my_nextfit_malloc(int size)
{
	mem_list *ret;
	
	if(_min_ == NULL)
	{
		ret = incHeap(size);
		_min_ = ret;
		_nextfit_ = _min_;
	}
	else
	{
		ret = nextFit(size);
		
		if(ret == NULL)
		{
			ret = incHeap(size);
		}
	}
	
	//printf("\nAlloc at %X - %X, Min = %X",ret, ret+1, _min_);
	return ret + 1;
	
	
}

mem_list* incHeap(int size)
{
	int m_size = size + sizeof(mem_list);
	
	mem_list *pref = sbrk(m_size);
	mem_list *pprev;
	
	if(pref == NULL)
	{
		return NULL;	
	}
	
	
	(*pref).ident = 0xFEA7;
	//printf("%X\n", (*pref).ident);
	(*pref).size = size;
	(*pref).next = NULL;
	(*pref).used = 1;	
	(*pref).prev = _max_;
	
	if(_max_ != NULL)
	{
		pprev = _max_;
		(*pprev).next = pref;
	}
	
	_max_ = pref;
	
	
	
	return pref;

}

mem_list* nextFit(int size)
{
	mem_list *next = _nextfit_;
	
	
	
	do
	{
		if(!(*next).used) //If not used
		{
			if((*next).size >= size) //And is large enough
			{
				//Good fit
				
				if((*next).size > size + sizeof(mem_list) + MIN_SIZE)  //Extra size
				{
					//Generate new mem_list
					mem_list *new = (void *)(next + 1) + size;
					
					//New node properties
					(*new).ident = 0xFEA7;
					(*new).next = (*next).next;
					(*new).prev = next;
					(*new).size = (*next).size - (size + sizeof(mem_list));
					(*new).used = 0;
					
					//Change existing node
					(*next).next = new;
					(*next).size = size;
					(*next).used = 1;
					
					//Change existing pointer
					mem_list *new_next = (*new).next;
					(*new_next).prev = new;
				}
				else
				{
					(*next).used = 1;
				}
				
				_nextfit_ = next;
				
				return next;
				
			}		
		}
		
		//Inc.
		if(next = _max_)
		{
			next = _min_;
		}
		else
		{
			next = (*next).next;
		}
	
	} while (next != _nextfit_);
	
	return NULL;
}

// Return values
// 0 if success
// -1 if not a malloced variable
int my_free(void* item)
{
	mem_list *free = (mem_list*)item - 1;
	
	if((*free).ident != 0xFEA7) return -1; //Not a valid address
	
	(*free).used = 0;
	


	
	//Check adjacent sections.
	mem_list *fprev = (*free).prev;
	mem_list *fnext = (*free).next;

	
	if(fprev != NULL) //Check previous
	{
		//printf("\n%X = %d\n",fprev, (*fprev).used);
		if((*fprev).used == 0)
		{
			//Move free to fprev
			(*fprev).next = fnext;
			(*fprev).size += (*free).size + sizeof(mem_list);
			//printf("\n%d + Old = %d\n",(*free).size, (*fprev).size);
			(*free).ident = 0x0; //Remove ident
			free = fprev;
			//printf("\nNew %d\n",(*free).size);
		}	
	}
	

	
	if(fnext != NULL) //Check next
	{
		if((*fnext).used == 0)
		{
			//Extend free to include fnext
			(*free).next = (*fnext).next;
			(*free).size += (*fnext).size + sizeof(mem_list);
			//printf("\n%d + Old = %d\n",(*fnext).size, (*free).size);
			(*fnext).ident = 0x0; //Remove ident
		}
	
	}


	
	//Check if heap should be shrunk
	int shrink;
	if((*free).next == NULL)
	{
		//If free is also the _min_
		if(free != _min_)
		{
			fprev = (*free).prev;
			_max_ = fprev;
			(*fprev).next = NULL;

			if(_nextfit_ > _max_) _nextfit_ = _min_;
		}
		else
		{
			_min_ = NULL;
			_max_ = NULL;
			_nextfit_ = NULL;
		}
		
		//printf("Old: %X  Size of free: %d\n", sbrk(0), (*free).size);
		shrink = -1*((*free).size + sizeof(mem_list));
		sbrk(shrink);
		//printf("After shrink of size %X: %X\n",-1*shrink,sbrk(0));
		
	}
	else
	{
		fnext = (*free).next;
		(*fnext).prev = free;
		_nextfit_ = free;
		//printf("\nFree at %X - %X\n",free, (*free).next);
	}
	

	
	
	
	return 0;
	
}

void WalkMem()
{
	
	printf("\n%X\n",_min_);
	
	if(_min_ != NULL) 
	
	{
	

	mem_list *walker = _min_;
	
	while (1)
	{
		printf("%X: Use:%d Size:%d Next:%X", walker, (*walker).used, (*walker).size, (*walker).next );
		printf("\n");
		if(walker == _max_) break;
		if((*walker).next == 0) break;
		walker = (*walker).next;
		
	}
	
	}
	printf("%X\n",sbrk(0));

}